const mqtt = require('mqtt')
const readline = require('readline')
const client = mqtt.connect('mqtt://localhost:1000')
const rdln = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var userName = 'Undefine';

client.on('connect', (err) => {
    console.log('Connected to Chat Server');
    client.subscribe('chat');
    rdln.question('Enter your name:', (answer) => {
        userName = answer;
        rdln.on('line', (input) => {
            client.publish('chat', userName + " says : " + input);
        });
    });
});

client.on('close', () => {
    console.log('Chat Server closed');
});

client.on('message', (topic, message) => {
    console.log(message.toString());
});