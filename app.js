const mosca = require('mosca')
const settings = {
    port: 1000
}
const client = new mosca.Server(settings)

client.on('ready', ()=>{
    console.log('Broker is ready!')
})

client.on('published', (packet)=>{
    message = packet.payload.toString()
    console.log(message)
})
